#include <assfire/api/v1/concepts/concepts.pb.h>
#include "assfire/concepts/Distance.hpp"
#include "assfire/concepts/Speed.hpp"
#include "assfire/concepts/TimeInterval.hpp"
#include "assfire/concepts/TimePoint.hpp"
#include "assfire/concepts/GeoPoint.hpp"

namespace assfire::proto
{
    template <typename TargetType, typename SourceType>
    TargetType proto_cast(const SourceType &obj)
    {
        static_assert(std::is_same<TargetType, SourceType>::value, "Please instantiate explicit proto_cast function for conversion of SourceType to TargetType");
        return obj;
    }

    using CppGeoPoint = assfire::GeoPoint;
    using ProtoGeoPoint = assfire::api::v1::concepts::GeoPoint;

    template <>
    CppGeoPoint proto_cast<CppGeoPoint, ProtoGeoPoint>(const ProtoGeoPoint &obj)
    {
        return CppGeoPoint::fromFixedPointLatLon(obj.lat(), obj.lon());
    }

    template <>
    ProtoGeoPoint proto_cast<ProtoGeoPoint, CppGeoPoint>(const CppGeoPoint &obj)
    {
        ProtoGeoPoint result;
        result.set_lat(obj.lat().toFixedPointInt());
        result.set_lon(obj.lon().toFixedPointInt());
        return result;
    }
}