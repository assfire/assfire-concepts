#pragma once

#include <cstdint>
#include <compare>
#include <cmath>
#include <limits>
#include "Coordinate.hpp"

namespace assfire {
    class GeoPoint {
    private:
        GeoPoint(Coordinate lat, Coordinate lon);

    public:
        GeoPoint operator+(const GeoPoint& rhs);
        GeoPoint operator-(const GeoPoint& rhs);
        GeoPoint& operator+=(const GeoPoint& rhs);
        GeoPoint& operator-=(const GeoPoint& rhs);
        GeoPoint& operator=(const GeoPoint& rhs);

        bool operator==(const GeoPoint& rhs) const = default;
        bool operator!=(const GeoPoint& rhs) const = default;
        
        const Coordinate& lat() const;
        const Coordinate& lon() const;

        static GeoPoint fromFloatingPointLatLon(double lat, double lon);
        static GeoPoint fromFixedPointLatLon(std::int64_t lat, std::int64_t lon);

    private:
        Coordinate _lat;
        Coordinate _lon;
    };
}