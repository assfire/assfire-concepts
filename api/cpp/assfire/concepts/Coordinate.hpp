#pragma once

#include <cstdint>

namespace assfire
{
    class Coordinate
    {
    private:
        explicit Coordinate(std::int64_t value);

    public:
        Coordinate();

        Coordinate(const Coordinate &rhs) = default;

        std::int64_t toFixedPointInt() const;

        double toFloatingPointDegrees() const;

        Coordinate operator+(const Coordinate &rhs) const;
        Coordinate operator-(const Coordinate &rhs) const;
        Coordinate& operator+=(const Coordinate &rhs);
        Coordinate& operator-=(const Coordinate &rhs);
        Coordinate& operator=(const Coordinate &rhs);

        bool operator==(const Coordinate &rhs) const;

        bool operator!=(const Coordinate &rhs) const;

        static Coordinate fromFixedPointInt(std::int64_t v);
        static Coordinate fromFloatingPointDegrees(double v);

    private:
        std::int64_t value;
    };
}