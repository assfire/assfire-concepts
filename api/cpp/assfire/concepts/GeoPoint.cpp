#include "GeoPoint.hpp"

namespace assfire
{
    GeoPoint::GeoPoint(Coordinate lat, Coordinate lon) : _lat(lat),
                                                         _lon(lon)
    {
    }

    GeoPoint GeoPoint::operator+(const GeoPoint &rhs)
    {
        return GeoPoint(lat() + rhs.lat(), lon() + rhs.lon());
    }

    GeoPoint GeoPoint::operator-(const GeoPoint &rhs)
    {
        return GeoPoint(lat() - rhs.lat(), lon() - rhs.lon());
    }

    GeoPoint &GeoPoint::operator+=(const GeoPoint &rhs)
    {
        _lat += rhs.lat();
        _lon += rhs.lon();
        return *this;
    }

    GeoPoint &GeoPoint::operator-=(const GeoPoint &rhs)
    {
        _lat -= rhs.lat();
        _lon -= rhs.lon();
        return *this;
    }

    GeoPoint &GeoPoint::operator=(const GeoPoint &rhs)
    {
        _lat = rhs.lat();
        _lon = rhs.lon();
        return *this;
    }

    const Coordinate &GeoPoint::lat() const
    {
        return _lat;
    }

    const Coordinate &GeoPoint::lon() const
    {
        return _lon;
    }

    GeoPoint GeoPoint::fromFloatingPointLatLon(double lat, double lon)
    {
        return GeoPoint(Coordinate::fromFloatingPointDegrees(lat), Coordinate::fromFloatingPointDegrees(lon));
    }

    GeoPoint GeoPoint::fromFixedPointLatLon(std::int64_t lat, std::int64_t lon)
    {
        return GeoPoint(Coordinate::fromFixedPointInt(lat), Coordinate::fromFixedPointInt(lon));
    }
}