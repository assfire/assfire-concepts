#include "Coordinate.hpp"

using namespace assfire;

namespace
{
    constexpr static double ENCODE_MULTIPLIER = 1e6;
}

Coordinate::Coordinate(std::int64_t value)
    : value(value) {}

Coordinate::Coordinate()
    : value(0) {}

bool Coordinate::operator==(const Coordinate &rhs) const
{
    return value == rhs.value;
}

bool Coordinate::operator!=(const Coordinate &rhs) const
{
    return !(rhs == *this);
}

Coordinate Coordinate::operator+(const Coordinate &rhs) const {
    return Coordinate(value + rhs.value);
}

Coordinate Coordinate::operator-(const Coordinate &rhs) const
{
    return Coordinate(value + rhs.value);
}

Coordinate& Coordinate::operator+=(const Coordinate &rhs) {
    value += rhs.value;
    return *this;
}

Coordinate& Coordinate::operator-=(const Coordinate &rhs)
{
    value -= rhs.value;
    return *this;
}

Coordinate& Coordinate::operator=(const Coordinate &rhs)
{
    value = rhs.value;
    return *this;
}

std::int64_t Coordinate::toFixedPointInt() const
{
    return value;
}

double Coordinate::toFloatingPointDegrees() const
{
    return value / ENCODE_MULTIPLIER;
}

Coordinate Coordinate::fromFixedPointInt(std::int64_t v)
{
    return Coordinate(v);
}

Coordinate Coordinate::fromFloatingPointDegrees(double v)
{
    return Coordinate(static_cast<std::int64_t>(v * ENCODE_MULTIPLIER));
}
